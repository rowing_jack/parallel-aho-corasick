// Data-parallel implementation of the Aho-Corasick algorithm in CUDA.
#include <algorithm>
#include <iostream>
#include <queue>

#include <cuda.h>

#include "../lib/ahocorasick.h"

namespace {
const int kChunkSize = 256;
const int kBlockDim = 256;

void NotifyExit(string message) {
  cout << message << "\n";
  exit(1);
}

// Print matches in a given string.
void MatchPatterns(const Trie *trie, string text, int nodes_cnt,
                   uint16_t alph_size) {
  int len = (int)text.size();
  int alphabet_size = (int)alph_size;

  // Initialize CUDA.
  CUresult status;
  cuInit(0);
  CUdevice cuDevice;
  if (cuDeviceGet(&cuDevice, 0) != CUDA_SUCCESS) {
    NotifyExit("CUDA initialization failed. 1");
  }
  CUcontext cuContext;
  if (cuCtxCreate(&cuContext, 0, cuDevice) != CUDA_SUCCESS) {
    NotifyExit("CUDA initialization failed. 2");
  }
  CUmodule cuModule = (CUmodule)0;
  if (cuModuleLoad(&cuModule, "ahocorasick_cuda.ptx") != CUDA_SUCCESS) {
    NotifyExit("CUDA initialization failed. 3");
  }
  CUfunction MatchPatterns;
  if (cuModuleGetFunction(&MatchPatterns, cuModule, "MatchPatterns") !=
      CUDA_SUCCESS) {
    NotifyExit("CUDA initialization failed. 4");
  }

  // Allocate host memory.
  int *state_transition, *state_fail, *output;
  char* text_c_str;
  status = cuMemAllocHost((void**)&state_transition,
                 sizeof(int) * nodes_cnt * alphabet_size);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating host memory failed.");
  }
  status = cuMemAllocHost((void**)&state_fail, sizeof(int) * nodes_cnt);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating host memory failed.");
  }
  status = cuMemAllocHost((void**)&output, sizeof(int) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating host memory failed.");
  }
  status = cuMemAllocHost((void**)&text_c_str, sizeof(char) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating host memory failed.");
  }
  for (int i = 0; i < len; i++) {
    text_c_str[i] = text[i];
  }

  // Compute the state_transition and state_fail tables.
  Node** state_to_node = new Node*[nodes_cnt];
  queue<Node*> Q;
  Q.push(trie->root);
  while (!Q.empty()) {
    Node* node = Q.front();
    Q.pop();
    if (node != trie->root) {
      state_fail[node->node_id] = (node->fail)->node_id;
    }
    state_to_node[node->node_id] = node;
    for (int i = 0; i < alphabet_size; i++) {
      int idx = node->node_id * alphabet_size + i;
      if (node->child[i] == nullptr) {
        state_transition[idx] = -1;
      } else {
        state_transition[idx] = (node->child[i])->node_id;
        Q.push(node->child[i]);
      }
    }
  }

  // Allocate device memory.
  CUdeviceptr dev_state_transition, dev_state_fail, dev_output, dev_text;
  status = cuMemAlloc(&dev_state_transition,
                      sizeof(int) * nodes_cnt * alphabet_size);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating device memory failed.");
  }
  status = cuMemAlloc(&dev_state_fail, sizeof(int) * nodes_cnt);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating device memory failed.");
  }
  status = cuMemAlloc(&dev_output, sizeof(int) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating device memory failed.");
  }
  status = cuMemAlloc(&dev_text, sizeof(char) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Allocating device memory failed.");
  }

  // Copy memory from host.
  status = cuMemcpyHtoD(dev_state_transition, state_transition,
                        sizeof(int) * nodes_cnt * alphabet_size);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Copying memory from host failed.");
  }
  status = cuMemcpyHtoD(dev_state_fail, state_fail, sizeof(int) * nodes_cnt);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Copying memory from host failed.");
  }
  status = cuMemcpyHtoD(dev_text, text_c_str, sizeof(char) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Copying memory from host failed.");
  }

  // Execute the kernel.
  void *args[] = {&dev_text, &dev_state_transition, &dev_state_fail,
                  &dev_output, &len, &alphabet_size};
  int num_threads = len / kChunkSize + 1;
  if (len % kChunkSize == 0) num_threads--;
  int num_blocks = num_threads / kBlockDim + 1;
  if (num_threads % kBlockDim == 0) num_blocks--;

  status = cuLaunchKernel(MatchPatterns, num_blocks, 1, 1, kBlockDim, 1, 1, 0,
                          0, args, 0);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Kernel execution failed.");
  }

  // Copy results from the device.
  status = cuMemcpyDtoH(output, dev_output, sizeof(int) * len);
  if (status != CUDA_SUCCESS) {
    NotifyExit("Copying results from the device failed.");
  }

  // Print the matches.
  for (int i = 0; i < len; i++) {
    if (output[i] != 0) {
      PrintMatches(trie, state_to_node[output[i]], i);
    }
  }

  // Cleanup.
  cuCtxDestroy(cuContext);
}
}

int main() {
  ios_base::sync_with_stdio(0);

  unsigned long num_patterns;
  uint16_t alphabet_size;
  string pattern, text;
  vector<string> patterns;

  cin >> num_patterns >> alphabet_size;
  Trie* trie = new Trie(alphabet_size);

  getline(cin, pattern);
  for (int i = 0; i < num_patterns; i++) {
    getline(cin, pattern);
    patterns.push_back(pattern);
  }
  int nodes_cnt = BuildTree(patterns, trie);

  getline(cin, text);
  MatchPatterns(trie, text, nodes_cnt, alphabet_size);
  delete trie;
  return 0;
}