// Data-parallel implementation of the Aho-Corasick algorithm in CUDA.
// This file contains kernel functions.

const int kChunkSize = 256;
const int kBlockDim = 256;

extern "C" {
__global__ void MatchPatterns(const char* text,
                              const int* state_transition,
                              const int* state_fail,
                              int* output,
                              int len,
                              int alph_size) {
  if ((blockIdx.x * kBlockDim + threadIdx.x) * kChunkSize > len) {
    return;
  }
  int state, new_state;
  int start = (blockIdx.x * kBlockDim + threadIdx.x) * kChunkSize;
  int stop = start;
  if (kChunkSize > (len - start)) {
    stop += len - start - 1;
  } else {
    stop += kChunkSize - 1;
  }

  state = new_state = 0;
  for (int i = start; i <= stop; i++) {
    while ((new_state = state_transition[state * alph_size + text[i]]) == -1
        && state != 0) {
      state = state_fail[state];
    }
    if (new_state != -1) state = new_state;
    output[i] = state;
  }
}
}
