// CPU implementation of the Aho-Corasick algorithm.
#include <iostream>

#include "../lib/ahocorasick.h"

namespace {
// Print matches in a given string.
void MatchPatterns(const Trie *trie, string text) {
  Node *node = trie->root;
  size_t len = text.size();
  for (int i = 0; i < len; i++) {
    node = Transition(trie, node, text[i]);
    PrintMatches(trie, node, i);
  }
}
}

int main() {
  ios_base::sync_with_stdio(0);

  unsigned long num_patterns;
  uint16_t alphabet_size;
  string pattern, text;
  vector<string> patterns;

  cin >> num_patterns >> alphabet_size;
  Trie* trie = new Trie(alphabet_size);

  getline(cin, pattern);
  for (int i = 0; i < num_patterns; i++) {
    getline(cin, pattern);
    patterns.push_back(pattern);
  }
  BuildTree(patterns, trie);

  getline(cin, text);
  MatchPatterns(trie, text);
  delete trie;
  return 0;
}