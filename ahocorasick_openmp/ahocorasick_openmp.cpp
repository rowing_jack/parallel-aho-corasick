// Data-parallel implementation of the Aho-Corasick algorithm in OpenMP.
#include <algorithm>
#include <iostream>

#include "../lib/ahocorasick.h"

namespace {
const int kChunkSize = 256;

// Print matches in a given string.
void MatchPatterns(const Trie *trie, string text) {
  Node* node;
  size_t len = text.size();
  int tid, i, start, stop;
  unsigned long num_threads = len / kChunkSize + 1;
  if (len % kChunkSize == 0) num_threads--;

#pragma omp parallel for shared(trie, text) firstprivate(len) private(tid, i, node, start, stop)
  for (tid = 0; tid < num_threads; tid++) {
    start = tid * kChunkSize;
    stop = start + min(kChunkSize, (int)len - start) - 1;

    node = trie->root;
    for (i = start; i <= stop; i++) {
      node = Transition(trie, node, text[i]);
      PrintMatches(trie, node, i);
    }
  }
}
}

int main() {
  ios_base::sync_with_stdio(0);

  unsigned long num_patterns;
  uint16_t alphabet_size;
  string pattern, text;
  vector<string> patterns;

  cin >> num_patterns >> alphabet_size;
  Trie* trie = new Trie(alphabet_size);

  getline(cin, pattern);
  for (int i = 0; i < num_patterns; i++) {
    getline(cin, pattern);
    patterns.push_back(pattern);
  }
  BuildTree(patterns, trie);

  getline(cin, text);
  MatchPatterns(trie, text);
  delete trie;
  return 0;
}