// Definition of Trie and Node structures.

#ifndef _TRIE_H
#define _TRIE_H

#include <cstdint>
#include <string>
#include <vector>
using namespace std;

class Node {
public:
  Node(char _chr, uint16_t alphabet_size);
  ~Node();

  Node *fail, *dict_fail;
  char chr;
  int pattern_num; // -1 by default, if the node doesn't represent a pattern.
  int node_id;
  vector<Node*> child;
};

class Trie {
public:
  Trie(uint16_t _alphabet_size);
  ~Trie();
  void Print();

  Node* root;
  uint16_t alphabet_size;
  vector<string> patterns;
};

#endif  // _TRIE_H
