// Definition of preprocessing and searching functions for the Aho-Corasick
// algorithm.

#ifndef _AHOCORASICK_H
#define _AHOCORASICK_H

#include "trie.h"

// Given a set of patterns, builds a new trie. Returns the number of nodes.
int BuildTree(const vector<string> patterns, Trie* tree);

// Performs transition to the next node in the trie.
Node* Transition(const Trie* trie, Node* node, char chr);

// Prints pattern matches ending at current position.
void PrintMatches(const Trie* trie, const Node* node, int pos);

#endif  // _AHOCORASICK_H
