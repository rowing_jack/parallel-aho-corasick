#include <iostream>
#include <queue>

#include "ahocorasick.h"

int BuildTree(const vector<string> patterns, Trie* trie) {
  uint16_t alphabet_size = trie->alphabet_size, pcnt = 0;
  int nodes_cnt = 1;
  // Building the trie structure.
  for (auto pattern : patterns) {
    trie->patterns.push_back(pattern);
    size_t len = pattern.size();
    Node *tmp = trie->root;
    for (int i = 0; i < len; i++) {
      if (tmp->child[pattern[i]] == nullptr) {
        tmp->child[pattern[i]] = new Node(pattern[i], alphabet_size);
        (tmp->child[pattern[i]])->node_id = nodes_cnt++;
      }
      tmp = tmp->child[pattern[i]];
    }
    tmp->pattern_num = pcnt++;
  }

  // Adding the fail and dict_fail links.
  Node* root = trie->root;
  queue<Node*> Q;
  for (int i = 0; i < alphabet_size; i++) {
    Node* chld = root->child[i];
    if (chld != nullptr) {
      Q.push(chld);
      chld->fail = root;
    }
  }
  while (!Q.empty()) {
    Node* node = Q.front();
    Q.pop();
    for (int i = 0; i < alphabet_size; i++) {
      Node* chld = node->child[i];
      if (chld != nullptr) {
        Q.push(chld);
        Node* tmp = node->fail;
        // Looking for the fail link.
        while (tmp != root && tmp->child[i] == nullptr) {
          tmp = tmp->fail;
        }
        bool found = (tmp->child[i] != nullptr);
        chld->fail = found ? tmp->child[i] : tmp;

        // Looking for the dictionary fail link.
        while (tmp != root && (tmp->child[i] == nullptr ||
            tmp->child[i]->pattern_num == -1)) {
          tmp = tmp->fail;
        }
        found = (tmp->child[i] != nullptr && tmp->child[i]->pattern_num != -1);
        chld->dict_fail = found ? tmp->child[i] : nullptr;
      }
    }
  }
  return nodes_cnt;
}

Node* Transition(const Trie* trie, Node* node, char chr) {
  while (node != trie->root && node->child[chr] == nullptr) {
    node = node->fail;
  }
  if (node->child[chr] != nullptr) {
    node = node->child[chr];
  }
  return node;
}

void PrintMatches(const Trie* trie, const Node* node, int pos) {
  if (node->pattern_num != -1) {
    cout << "Found pattern(" << trie->patterns[node->pattern_num] << ") " <<
    "ending at position(" << pos << ")\n";
  }
  Node* tmp = node->dict_fail;
  while (tmp != nullptr) {
    cout << "Found pattern(" << trie->patterns[tmp->pattern_num] << ") " <<
    "ending at position(" << pos << ")\n";
    tmp = tmp->dict_fail;
  }
}
