#include <iostream>

#include "trie.h"

Node::Node(char _chr, uint16_t _alphabet_size)
    : chr(_chr),
      fail(nullptr),
      dict_fail(nullptr),
      pattern_num(-1),
      node_id(0),
      child(vector<Node*>(_alphabet_size, nullptr)) {
}

Node::~Node() {
  child.clear();
}

Trie::Trie(uint16_t _alphabet_size) : alphabet_size(_alphabet_size) {
  root = new Node('\0', alphabet_size);
}

Trie::~Trie() {
  delete root;
  patterns.clear();
}

void DFSPrint(Node *node, uint16_t alphabet_size, int depth) {
  for (int i = 0; i < depth; i++) printf(" ");
  cout << "chr(" << node->chr << "), pattern_nu(" << node->pattern_num << ")\n";
  for (int i = 0; i < alphabet_size; i++) {
    if (node->child[i] != nullptr) {
      DFSPrint(node->child[i], alphabet_size, depth + 1);
    }
  }
}

void Trie::Print() {
  DFSPrint(root, alphabet_size, 0);
}